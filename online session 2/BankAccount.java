package pro.bcas.bank;

public class BankAccount {
	public static final String bankName = "BOC Bank";
	private String branch;
	private double accBalance = 0;
	private String accType;
	private String accNumber;
	private String accHolderName;
	
	public void openAccount(String branch,String accName,String accNumber,String accHolderName,int amount) {
		
		this.branch = branch;
		this.accType = accName;
		this.accNumber = accNumber;
		this.accHolderName = accHolderName;
		this.accBalance = amount;
		
	}
	
	public void withdrawal (double amount) {
		accBalance = accBalance + amount;
		
	}
	public void deposit(double amount) {
		accBalance = accBalance + amount;
		
	}
	public double getAccBlance() {
		return accBalance;
		
	}
	public String getAccountHolderName() {
		return accHolderName;
		
	}
	public void getAccountDetatils() {
		
		System.out.println("Bank Name : " + bankName + "-"+branch);
		System.out.println("Account Number : " + accNumber);
		System.out.println("Account Type : " + accType);
		System.out.println("Account Holder Name : " + accHolderName);
		System.out.println("Account Balance : " + accBalance);
		System.out.println ("................................");
	}
}
