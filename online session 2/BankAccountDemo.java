package pro.bcas.bank;

public class BankAccountDemo {
	public static void main (String[]args) {
		
		BankAccount accKathuSha = new BankAccount();
		BankAccount accKajeenthan = new BankAccount();
		BankAccount accKathu = new BankAccount();
		BankAccount accThuSha = new BankAccount();
		
		accKathuSha.openAccount("Jaffna","Savings","794386549","K.KathuSha",10000);  
		accKajeenthan.openAccount("Jaffna","Savings","794386543","K.Kajieenthan",200000);  
		accKathu.openAccount("PointPedro","Savings","987586549","K.Kathu",100);  
		accThuSha.openAccount("Jaffna","Savings","876309749","K.ThuSha",8000);  
		
		accKathu.getAccountDetatils();
		accKajeenthan.getAccountDetatils();
		accKathuSha.getAccountDetatils();
		accThuSha.getAccountDetatils();
		
		accThuSha.deposit(10000);
		accKajeenthan.withdrawal(5000);
		accKathuSha.withdrawal(7000);
		accKathu.deposit(10000);
		
		System.out.println(accThuSha.getAccountHolderName() + "S account balance = " + accThuSha.getAccBlance());
		System.out.println(accKajeenthan.getAccountHolderName() + "S account balance = " + accKajeenthan.getAccBlance());
		System.out.println(accKathuSha.getAccountHolderName() + "S account balance = " + accKathuSha.getAccBlance());
		System.out.println(accKathu.getAccountHolderName() + "S account balance = " + accKathu.getAccBlance());
		
		
		
		
		
		
	}
	
	

}

Bank Name : BOC Bank-PointPedro
Account Number : 987586549
Account Type : Savings
Account Holder Name : K.Kathu
Account Balance : 100.0
................................
Bank Name : BOC Bank-Jaffna
Account Number : 794386543
Account Type : Savings
Account Holder Name : K.Kajieenthan
Account Balance : 200000.0
................................
Bank Name : BOC Bank-Jaffna
Account Number : 794386549
Account Type : Savings
Account Holder Name : K.KathuSha
Account Balance : 10000.0
................................
Bank Name : BOC Bank-Jaffna
Account Number : 876309749
Account Type : Savings
Account Holder Name : K.ThuSha
Account Balance : 8000.0
................................
K.ThuShaS account balance = 18000.0
K.KajieenthanS account balance = 205000.0
K.KathuShaS account balance = 17000.0
K.KathuS account balance = 10100.0

