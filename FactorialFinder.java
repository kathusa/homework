public class FactorialFinder {
	
		public  int performFactorial (int fact){
			
			//3!= 1 x 2 x 3
			int factValue = 1;
			for (int i = 1; i <= fact; i++){
				factValue = factValue * i;
			}
		return factValue;
	}
}
