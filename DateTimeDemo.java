import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
public class DateTimeDemo {
	
	public static void main(String[] args) {
		System.out.println("Current Date & Time");
		
			LocalDate date = LocalDate.now();
			System.out.println(date);
			
			LocalTime time = LocalTime.now();
			System.out.println(time);
			
			LocalDate dateTime = LocalDate.now();
			System.out.println(dateTime);
			
			ZoneId zoneIdSin = ZoneId.of("Singapore");
			LocalTime SingTime = LocalTime.now(zoneIdSin);
			System.out.println("Singapore : "+SingTime);
			
			ZoneId zoneIdYK = ZoneId.of("America/Yellowknife");
			LocalTime YKTime = LocalTime.now(zoneIdYK);
			System.out.println("America/Yellowknife : "+YKTime);
			
			ZoneId zoneIdSam = ZoneId.of(" US/Samoa");
			LocalTime SamTime = LocalTime.now(zoneIdSam);
			System.out.println(" US/Samoa : "+SamTime);
			
			ZoneId zoneIdAtk = ZoneId.of("America/Atka ");
			LocalTime AtkTime = LocalTime.now(zoneIdAtk);
			System.out.println(" America/Atka : "+AtkTime);
			
			ZoneId zoneIdDar = ZoneId.of(" Australia/Darwin  ");
			LocalTime DarTime = LocalTime.now(zoneIdDar);
			System.out.println("  Australia/Darwin : "+DarTime);
			
			ZoneId zoneIdDil = ZoneId.of(" Asia/Dili  ");
			LocalTime DilTime = LocalTime.now(zoneIdDil);
			System.out.println("  Asia/Dili : "+DilTime);
			
			
		}
	}
